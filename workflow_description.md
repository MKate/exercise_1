Pracując z Git-em nieodłączne jest tworzenie gałęzi w ramach których rozwijamy nowe funkcjonalności, naprawiamy błędy, przygotowujemy wydania, przeprowadzamy refaktoring i wiele więcej. We wszystkich tych przypadkach schemat działania jest ten sam:

- przechodzimy do gałęzi głównej MASTER,
- tworzymy nową gałąź,
- wprowadzamy modyfikacje,
- wracamy do gałęzi głównej MASTER,
- scalamy modyfikacje,
- usuwamy zbędną gałąź
I tak w kółko :) 

Gałąź MASTER – przechowuje tylko wersję finalną i oznaczana jest tag’iem, czyli numerem wersji.
DEVELOPMENT – zawiera wersję developerską, czyli taką, do której wprowadzane są zmiany.
FEATURE branch'e wykorzystujemy do obsługi nowych funkcjonalności w projekcie. Nie wiemy kiedy dokładnie dana funkcjonalność wejdzie na produkcje (kiedy będziemy mogli połączyć ją z MASTER), dlatego FEATURE branch'a musimy utworzyć z branch'a DEVELOPMENT.

Tworzenie feature branch’a będąc w branchu develop wykonujemy poleceniem:
- git checkout -b feature\myfeature development

Kończąc pracę nad daną funkcjonalnością możemy go zmargować do brancha develop następującymi poleceniami:
- git checkout develop
- git merge –no-ff feature\myfeature
- git branch -d feature\myfeature
- git push origin develop

Po co używać flagi –no-ff margując branch’e? Zwykły marge wcieli nam wszystkie zmiany do brancha develop jednak w takim przypadku utracimy informacje o istnieniu branch’a feature.

–no –ff – czyli “no fast forward” ustawienie tych flag powoduje utworzenie pustego commita w przypadku, gdy nie było zmian w develop’ie. 
